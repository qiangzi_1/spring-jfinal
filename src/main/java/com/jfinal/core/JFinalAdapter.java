package com.jfinal.core;

/**
 * JFinal 适配器(主要原因是JFinal作用于太小了)
 * 使用 adapter 必须在 SpringJFinalFilter 之后
 * @ClassName: JFinalAdapter 
 * @author huangx
 * @date 2015年2月6日 上午9:05:26
 */
public class JFinalAdapter {
	private JFinal jfinal;
	private static SpringJFinal srpingJFinal;

	protected JFinalAdapter(JFinal jfinal) {
		this.jfinal = jfinal;
	}

	public static SpringJFinal getSrpingJFinal(JFinal jfinal) {
		if (srpingJFinal == null) {
			synchronized (JFinalAdapter.class) {
				if (srpingJFinal == null) {
					srpingJFinal = new AbstractSpringJFinal(jfinal){};
				}
			}
		}
		return srpingJFinal;
	}

}

package com.jfinal.core;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;

import com.jfinal.core.Action;
import com.jfinal.core.Controller;
import com.jfinal.handler.Handler;
import com.jfinal.util.ReflectUtils;
import com.jfinal.util.SpringUtils;

/**
 * spring 必要的 handler
 * @ClassName: AbstractSpringHandler 
 * @author huangx
 * @date 2015年2月4日 下午12:01:57
 */
public abstract class AbstractSpringHandler extends Handler {

	// spring
	private ApplicationContext ctx;

	public AbstractSpringHandler(ApplicationContext ctx) {
		Assert.notNull(ctx);
		this.ctx = ctx;
	}

	/**
	 * @Title: 获得 Controller
	 * @param controllerClass
	 * @return
	 */
	public Controller getController(Class<? extends Controller> controllerClass) {
		Assert.notNull(controllerClass);
		String controllerBeanName = controllerClass.getName();
		if (!ctx.containsBean(controllerBeanName)) {
			// 动态注册
			BeanDefinitionBuilder builder = SpringUtils.genericBean(controllerClass);
			AbstractBeanDefinition beanDefinition = builder.getRawBeanDefinition();
			// 每次请求都创建一个实例 符合 jfinal 创建 Controller方式 (jfinal 是每次请求都创建实例).
			beanDefinition.setScope(SpringUtils.SCOPE_PROTOTYPE);
			SpringUtils.registerBean(ctx, controllerBeanName, beanDefinition);
		}
		// controller 
		return ctx.getBean(controllerClass);
	}

	protected void invoke(Action action, Controller controller) {
		Class<?> invocationClass = null;
		try {
			invocationClass = ReflectUtils.forName("com.jfinal.core.ActionInvocation");
		} catch (Exception e) {
			// ignore Exception
			// JFinal 2.*
			invocationClass = ReflectUtils.forName("com.jfinal.aop.Invocation");
		}
		try {
			Object invocation = invocationClass.getDeclaredConstructor(Action.class, Controller.class).newInstance(action,
					controller);
			invocationClass.getMethod("invoke").invoke(invocation);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}

package com.jfinal.core;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;

import com.jfinal.config.JFinalConfig;

public interface SpringJFinal {
	boolean init(JFinalConfig jfinalConfig, ServletContext servletContext);

	void initApplicationContext(FilterConfig filterConfig);

	Object registerJFinalBean(String configClass);
}

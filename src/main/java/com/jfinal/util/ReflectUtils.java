package com.jfinal.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Reflect
 * 
 * @ClassName: ReflectUtils
 * @author huangx
 * @date 2015年1月30日 下午2:58:01
 */
@SuppressWarnings("unchecked")
public class ReflectUtils {

	/*-------------------------------- Field -----------------------------------*/
	/**
	 * @Title: 赋值 field
	 * @param f
	 *            Field
	 * @param obj
	 *            对象
	 * @param value
	 *            值
	 */
	public static void set(Object obj, Field f, Object value) {
		try {
			f.set(obj, value);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @Title: 获得值
	 * @param f
	 *            Field
	 * @param obj
	 *            对象
	 * @return 值
	 */
	public static <T> T get(Object obj, Field f) {
		try {
			return (T) f.get(obj);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @Title: 获得 Field
	 * @param cls
	 *            Class
	 * @param name
	 *            名称
	 * @return Field
	 */
	public static Field field(Class<?> cls, String name) {
		try {
			return cls.getField(name);
		} catch (Exception e) {
			try {
				Field f = cls.getDeclaredField(name);
				f.setAccessible(true);
				return f;
			} catch (Exception e1) {
				throw new IllegalArgumentException(e1);
			}
		}
	}

	/*-------------------------------- Instance -----------------------------------*/
	/**
	 * @Title: 新建 实例
	 * @param cls
	 * @return
	 */
	public static <T> T newInstance(Class<T> cls) {
		try {
			return cls.newInstance();
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/*-------------------------------- Method -----------------------------------*/
	/**
	 * @Title: 获得 Method
	 * @param cls
	 * @param name
	 * @param parameterTypes
	 * @return Method
	 * @throws
	 */
	public static Method method(Class<?> cls, String name, Class<?>... parameterTypes) {
		try {
			return cls.getMethod(name, parameterTypes);
		} catch (Exception e) {
			try {
				Method m = cls.getDeclaredMethod(name, parameterTypes);
				m.setAccessible(true);
				return m;
			} catch (Exception e1) {
				throw new IllegalArgumentException(e1);
			}
		}
	}

	/**
	 * @Title: 执行 Method
	 * @param m
	 * @param obj
	 * @param args
	 * @return T
	 * @throws
	 */
	public static <T> T invoke(Object obj, Method m, Object... args) {
		try {
			return (T) m.invoke(obj, args);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @Title: 执行 Method
	 * @param obj
	 * @param name
	 * @param args (args 中不能有 null 值)
	 * @return
	 */
	public static <T> T invoke(Object obj, String name, Object... args) {
		return invoke(obj, name, obj.getClass(), args);
	}

	/**
	 * @Title: 执行 Method
	 * @param obj
	 * @param name
	 * @param cls
	 * @param args (args 中不能有 null 值)
	 * @return
	 */
	public static <T> T invoke(Object obj, String name, Class<?> parentClass, Object... args) {
		Class<?>[] parameterTypes = new Class[args.length];
		for (int i = 0; i < parameterTypes.length; i++) {
			parameterTypes[i] = args[i].getClass();
		}
		if (parentClass == null) parentClass = obj.getClass();
		Method m = method(parentClass, name, parameterTypes);
		return invoke(obj, m, args);
	}

	/*-------------------------------- Class -----------------------------------*/
	/**
	 * @Title: toClass
	 * @param className
	 * @return
	 */
	public static <T> Class<T> forName(String className) {
		try {
			return (Class<T>) Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @Title: toClasses
	 * @param classNames
	 * @return
	 */
	public static Class<?>[] forNames(String... classNames) {
		int length = 0;
		if (classNames != null) length = classNames.length;
		Class<?>[] classes = new Class[length];
		for (int i = 0; i < length; i++) {
			classes[i] = forName(classNames[i]);
		}
		return classes;
	}

	/**
	 * @Title: 获得 class 泛型实际类型
	 * @param classes
	 * @return
	 */
	public static <T> Class<T> getParamTypeClass(Class<?> classes) {
		return getParamTypeClass(classes, 0);
	}

	/**
	 * @Title: 获得 class 泛型实际类型
	 * @param classes
	 * @param idx
	 * @return
	 */
	public static <T> Class<T> getParamTypeClass(Class<?> classes, int idx) {
		// 得到泛型父类
		Type genType = classes.getGenericSuperclass();
		// 返回表示此类型实际类型参数的Type对象的数组,数组里放的都是对应类型的Class
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();

		if (idx < 0 || idx > params.length - 1) throw new ArrayIndexOutOfBoundsException(idx);
		return (Class<T>) params[idx];
	}
}

package com.jfinal.spring.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;

import com.jfinal.handler.Handler;

/**
 * spring 上下文 handler
 * Example:<br>
 * In JFinalFilter: handlers.add(new ContextSpringHandler("SPRING_CONTEXT",applicationContext));<br>
 * in ${SPRING_CONTEXT} />
 * @ClassName: ContextSpringHandler 
 * @author huangx
 * @date 2015年2月5日 上午10:43:31
 */
public class ContextSpringHandler extends Handler {

	private String springContextName;
	private ApplicationContext ctx;

	public ContextSpringHandler(ApplicationContext ctx) {
		this("SPRING_CONTEXT", ctx);
	}

	public ContextSpringHandler(String springContextName, ApplicationContext cxt) {
		Assert.notNull(this.springContextName = springContextName, "springContextName can not be blank.");
		Assert.notNull(this.ctx = cxt, "ApplicationContext can not be blank.");
	}

	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		request.setAttribute(springContextName, ctx);
		nextHandler.handle(target, request, response, isHandled);
	}
}

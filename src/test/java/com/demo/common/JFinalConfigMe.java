package com.demo.common;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;

public class JFinalConfigMe extends JFinalConfig {
	private ApplicationContext ctx;
	
	// lazy (原因是 jfinal 先实例化 JFinalConfig 在 JFinal.init 初始化相关配置 )
	public ApplicationContext getApplicationContext(){
		if(ctx == null) ctx = WebApplicationContextUtils.getWebApplicationContext(JFinal.me().getServletContext());
		return ctx;
	}

	@Override
	public void configConstant(Constants me) { }
	@Override
	public void configRoute(Routes me) { }
	@Override
	public void configPlugin(Plugins me) {
	}
	@Override
	public void configInterceptor(Interceptors me) { }
	@Override
	public void configHandler(Handlers me) { }
}

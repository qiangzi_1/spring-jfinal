package com.demo.common;

import javax.sql.DataSource;

import net.sf.log4jdbc.Log4jdbcProxyDataSource;
import net.sf.log4jdbc.tools.Log4JdbcCustomFormatter;
import net.sf.log4jdbc.tools.LoggingType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.demo.controller.BlogController;
import com.demo.model.BlogModel;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.AnsiSqlDialect;
import com.jfinal.render.ViewType;

public class CommonConfig extends JFinalConfig {
	private static Logger log = LoggerFactory.getLogger(CommonConfig.class);

	private ApplicationContext ctx;

	@Autowired
	public void setApplicationContext(ApplicationContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void configConstant(Constants me) {
		log.info("configConstant 缓存 properties");
		loadPropertyFile("common.properties");

		log.info("configConstant 设置字符集");
		me.setEncoding("UTF-8");

		log.info("configConstant 设置是否开发模式");
		me.setDevMode(getPropertyToBoolean("devMode", true));
		me.setViewType(ViewType.JSP);
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/blog", BlogController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		Boolean devMode = getPropertyToBoolean("devMode", true);// 开发模式
		DataSource dataSource = ctx.getBean("dataSourceProxy", DataSource.class);
		if (devMode) dataSource = log4jdbcProxyDataSource(dataSource);
		
		log.info("configPlugin 配置ActiveRecord插件");
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dataSource);
		arp.setDevMode(devMode); // 设置开发模式 

		log.info("configPlugin 使用数据库类型是 sql server");
		arp.setDialect(new AnsiSqlDialect());
		arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));// 小写
		// mapping
		arp.addMapping("blog", BlogModel.class);

		log.info("configPlugin 添加druidPlugin插件");
		me.add(arp); // 
	}

	@Override
	public void configInterceptor(Interceptors me) {
	}

	@Override
	public void configHandler(Handlers me) {}

	/*-------------------------------------------------------------*/
	static DataSource log4jdbcProxyDataSource(DataSource ds) {
		Log4JdbcCustomFormatter format = new Log4JdbcCustomFormatter();
		format.setLoggingType(LoggingType.MULTI_LINE);
		format.setSqlPrefix("SQL:::");
		Log4jdbcProxyDataSource proxy = new Log4jdbcProxyDataSource(ds);
		proxy.setLogFormatter(format);
		return proxy;
	}	

	public static void main(String[] args) {
		//直接在这个页面启动，在浏览器中输入localhost/index 进行访问
		JFinal.start("src/main/webapp", 80, "/", 5);
	}

}

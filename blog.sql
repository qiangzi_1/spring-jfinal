/*
Navicat MySQL Data Transfer

Source Server         : (mariadb)local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2015-04-07 14:32:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,
  `publishTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES ('1', 'JFinal Demo Title here', 'JFinal Demo Content here', null);
INSERT INTO `blog` VALUES ('7', '标题', '', null);
INSERT INTO `blog` VALUES ('8', '标题1', '内容11', null);
INSERT INTO `blog` VALUES ('9', '标题2', '内容2', '2015-03-30 14:49:56');
INSERT INTO `blog` VALUES ('10', '标题3', '内容3', '2015-03-30 15:08:23');
INSERT INTO `blog` VALUES ('11', '标题3', '内容3', '2015-03-30 15:09:33');
INSERT INTO `blog` VALUES ('13', 'asfasf', 'safsdfsa', '2015-03-30 16:13:13');
INSERT INTO `blog` VALUES ('14', 'ibatis title', 'update content1', '2015-03-31 10:05:05');

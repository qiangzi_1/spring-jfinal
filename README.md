#spring-jfinal
作用于 spring 和 jfinal 的整合。
## 1、web.xml
```xml
	<context-param>
		<param-name>contextConfigLocation</param-name>
		<param-value>WEB-INF/spring.xml</param-value>
	</context-param>
	<listener>
		<listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
	</listener>
    <filter>
        <filter-name>jfinal</filter-name>
    	<filter-class>com.jfinal.core.SpringJFinalFilter</filter-class>
    	<init-param>
    		<param-name>configClass</param-name>
    		<param-value>demo.run.JfinalConfig</param-value>
    	</init-param>
    </filter>
    <filter-mapping>
    	<filter-name>jfinal</filter-name>
    	<url-pattern>/*</url-pattern>
    </filter-mapping>
```
以上通过 SpringJFinalFilter 过滤器加载 spring 。
其中 JfinalConfig 自动注册 springbean（JfinalConfig 中可以使用 注解注入属性哦，或者 实现 ApplicationContextAware  接口自动注入 ApplicationContext）。

初始化顺序（不配置 ContextLoaderListener 的情况下）:
	SpringJFinalFilter -> ApplicationContext -> JFinal -> JFinalConfig ... (其他 照旧 ) .
初始化顺序（配置 ContextLoaderListener 的情况下）。
	ApplicationContext -> SpringJFinalFilter -> JFinal -> JFinalConfig ... (其他 照旧 ) .

## 2、JFinalConfig
``` java
	public class HelloJFinalConfig extends JFinalConfig {
	
		@Autowired
		public void setApplicationContext(ApplicationContext ctx) {
			System.out.println("HelloController attr[ApplicationContext] 已注入。。");
		}
		...
	}
```
其中 HelloJFinalConfig 自动注入 springbean , scope = "singleton" .

## 3、Controller
``` java
	public class HelloController extends Controller {
	
		@Autowired
		public void setApplicationContext(ApplicationContext ctx) {
			System.out.println("HelloController attr[ApplicationContext] 已注入。。");
		}
	}
```
其中 Controller 自动注入 springbean , scope = "prototype" 原因是 jfinal 也是每次请求创建实例 .

## 4、页面使用 ApplicationContext
``` text
添加 ContextSpringHandler 就可以了。使用方式可以参照 ContextPathHandler。
```

## 5、spring 事物管理
```xml
    <bean id="druidDataSource" class="com.alibaba.druid.pool.DruidDataSource">
		<property name="username" value="${db.userName}" />
		<property name="password" value="${db.passWord}" />
		<property name="url" value="${db.jdbcUrl}" />
		<property name="driverClassName" value="${db.driverClassName}" />
		<property name="initialSize" value="${db.initialSize}" />
		<property name="maxActive" value="${db.maxActive}" />
		<property name="minIdle" value="${db.minIdle}" />
	</bean>
	<!-- spring 事物管理 ,ActiveRecordPlugin可以获得此 dataSource 可以把事务交给spring 管理 -->
	<bean id="dataSourceProxy" class="org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy">
		<property name="targetDataSource" ref="druidDataSource" />
	</bean>

	<!-- ================================事务相关控制================================================= -->
	<bean name="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
		<property name="dataSource" ref="dataSourceProxy"></property>
	</bean>

	<tx:advice id="txAdvice" transaction-manager="transactionManager">
		<tx:attributes>
			<tx:method name="delete*" propagation="REQUIRED" read-only="false" />
			<tx:method name="insert*" propagation="REQUIRED" read-only="false" />
			<tx:method name="update*" propagation="REQUIRED" read-only="false" />

			<tx:method name="find*" propagation="SUPPORTS" />
			<tx:method name="get*" propagation="SUPPORTS" />
			<tx:method name="select*" propagation="SUPPORTS" />
		</tx:attributes>
	</tx:advice>

	<!-- 把事务控制在Service层 -->
	<aop:config>
		<aop:pointcut id="pc" expression="execution(public * demo.service.*.*(..))" />
		<aop:advisor pointcut-ref="pc" advice-ref="txAdvice" />
	</aop:config>
```
以上是 xml 配置方式。 重点是在 ActiveRecordPlugin 中注入 代理数据源。

## 6、基于 SpringPlugin 的 加载 spring （此插件 JFinal v2.* 已经移除）.
```java
	public class JFinalConfigMe extends JFinalConfig {
		private ApplicationContext ctx;
		
		// lazy (原因是 jfinal 先实例化 JFinalConfig 在 JFinal.init 初始化相关配置 )
		public ApplicationContext getApplicationContext(){
			if(ctx == null) ctx = WebApplicationContextUtils.getWebApplicationContext(JFinal.me().getServletContext());
			return ctx;
		}
	
		@Override
		public void configConstant(Constants me) { }
		@Override
		public void configRoute(Routes me) { }
		@Override
		public void configPlugin(Plugins me) {
			me.add(new SpringPlugin(getApplicationContext()));
		}
		@Override
		public void configInterceptor(Interceptors me) { }
		@Override
		public void configHandler(Handlers me) { }
	}
```

- 可以使用 加载 WEB-INF 下的文件哦（需要配置 ContextLoaderListener） 推荐.
- 如果不配置 ContextLoaderListener 使用 WEB-INF 下的文件 的 classpath:../**文件。 

## 7、更多支持
- spring-jfinal [下载地址](http://pan.baidu.com/s/1i3s2UB7)
- oschina [开源社区 ](http://www.oschina.net/p/spring-jfinal)
- JFinal [官方网站 ](http://www.jfinal.com/) 
- 关注官方微信号马上体验 demo 功能  
![JFinal Weixin SDK](http://static.oschina.net/uploads/space/2015/0211/181947_2431_201137.jpg)